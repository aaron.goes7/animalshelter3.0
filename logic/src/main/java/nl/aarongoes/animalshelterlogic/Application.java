package nl.aarongoes.animalshelterlogic;

import nl.aarongoes.animalshelterlogic.animals.Animal;
import nl.aarongoes.animalshelterlogic.animals.Gender;
import nl.aarongoes.animalshelterlogic.animals.Species;
import nl.aarongoes.animalshelterlogic.interfaces.Sellable;
import nl.aarongoes.animalshelterlogic.storage.ShelterStorage;

import java.io.IOException;
import java.util.List;

public class Application {
    private Shelter shelter;
    private ShelterStorage shelterStorage;
    private Observable observable;

    public Application(){
        shelterStorage = new ShelterStorage();
        observable = new Observable();
        shelter = new Shelter();
        loadFromSave();
    }

    public void subscribe(Observer observer){
        observable.subscribe(observer);
    }

    public List<Animal> getAnimals(){
        return shelter.getAnimals();
    }

    public List<Sellable> getItems(){
        return shelter.getItems();
    }

    public void addAnimal(Species species, String name, Gender gender, String badHabits){
        shelter.addAnimal(species, name, gender, badHabits);
        saveAndUpdate();
    }

    public void sellAnimal(Animal animal){
        shelter.sellAnimal(animal);
        saveAndUpdate();
    }

    public void sellItem(Product item){
        shelter.sellItem(item);
        saveAndUpdate();
    }

    public void buyItem(Sellable item){
        shelter.buyItem(item);
        saveAndUpdate();
    }

    public void reserveAnimal(Animal animal, String reservor){
        animal.reserve(reservor);
        saveAndUpdate();
    }

    private void saveAndUpdate(){
        try {
            shelterStorage.saveAnimalShelter(shelter);
        } catch (IOException e) {
            e.printStackTrace();
        }
        observable.updateObservers();
    }

    public void loadFromSave(){
        try {
            this.shelter = this.shelterStorage.getSavedAnimalShelter();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
