package nl.aarongoes.animalshelterlogic;

import java.util.ArrayList;
import java.util.List;

public class Observable {
    private List<Observer> observers;

    public Observable(){
        observers = new ArrayList<>();
    }

    public void subscribe(Observer observer){
        observers.add(observer);
        updateObservers();
    }

    public void updateObservers(){
        for (Observer observer : observers){
            observer.update();
        }
    }
}
