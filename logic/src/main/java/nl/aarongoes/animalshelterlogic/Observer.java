package nl.aarongoes.animalshelterlogic;

public interface Observer {
    public void update();
}
