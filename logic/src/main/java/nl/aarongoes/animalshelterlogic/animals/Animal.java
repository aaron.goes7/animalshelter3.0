package nl.aarongoes.animalshelterlogic.animals;

//import lombok.Getter;
import nl.aarongoes.animalshelterlogic.interfaces.Sellable;

import java.io.Serializable;
import java.util.Date;

public abstract class Animal implements Serializable, Sellable{
    public int price;
    public String name;
    public Gender gender;
    public Reservor reservedBy;

    public Animal(String name, Gender gender){
        this.name = name;
        this.gender = gender;
    }

    public void setPrice(int price){
        this.price = price;
    }

    public int getPrice(){
        return this.price;
    }

    public boolean reserve(String reservedBy){
        if (this.reservedBy == null){
            this.reservedBy = new Reservor(reservedBy, new Date());
            return true;
        }
        return false;
    }

    public String toString(){
        String reserved = "not reserved";
        if (this.price != 0){
            return this.name + " sold for: " + this.price;
        }
        if (this.reservedBy != null){
            reserved = "reserved by " + this.reservedBy;
        }
        return this.name + " " + this.gender  + " " + reserved;
    }
}
