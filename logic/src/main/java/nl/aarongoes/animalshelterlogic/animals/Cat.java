package nl.aarongoes.animalshelterlogic.animals;

//import lombok.Getter;

import java.io.Serializable;

public class Cat extends Animal implements Serializable {
//    @Getter
    public String badHabits;

    public Cat(String name, Gender gender, String badHabits){
        super(name, gender);
        this.badHabits = badHabits;
    }

    public String getBadHabits(){
        return badHabits;
    }

    public String toString(){
        return super.toString() + ", bad habits: " + this.badHabits.toLowerCase();
    }
}
