package nl.aarongoes.animalshelterlogic.animals;

//import lombok.Getter;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Reservor implements Serializable {
//    @Getter
    public String name;
//    @Getter
    public Date reservedAt;

    public Reservor(String name, Date reservedAt){
        this.name = name;
        this.reservedAt = reservedAt;
    }

    public String toString(){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        return this.name + " on " + df.format(this.reservedAt);
    }
}
