package nl.aarongoes.animalshelterlogic.animals;

import java.io.Serializable;

public enum Species implements Serializable {
    Dog(0),
    Cat(1);

    private int type;

    Species(int i) {
        this.type = i;
    }

    int getType(){
        return this.type;
    }
}
