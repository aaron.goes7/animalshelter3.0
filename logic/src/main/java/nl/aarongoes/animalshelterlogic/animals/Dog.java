package nl.aarongoes.animalshelterlogic.animals;

//import lombok.Getter;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Dog extends Animal implements Serializable {
//    @Getter
    public Date lastWalk;
    public boolean needsWalk(){
        long diffInMillies = Math.abs(lastWalk.getTime() - new Date().getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        if (diff > 0){
            return true;
        }
        return false;
    }
    public Dog(String name, Gender gender){
        super(name, gender);
        this.lastWalk = new Date();
    }

    public String toString(){
        DateFormat df = new SimpleDateFormat("HH:mm");
        return super.toString() + ", last walk: " + df.format(this.lastWalk);
    }
}
