package nl.aarongoes.animalshelterlogic.animals;

import java.io.Serializable;

public enum Gender implements Serializable {
    male,
    female
}
