package nl.aarongoes.animalshelterlogic.storage;
import nl.aarongoes.animalshelterlogic.Shelter;

import java.io.*;

public class ShelterStorage {

    private final File shelterFile = new File("shelter.txt");

    private void serializeObject(Object o) throws IOException {
        try {
            FileOutputStream fos = new FileOutputStream(shelterFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private <T> T deserializeData(Class<T> cls, String path) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(path);
        ObjectInputStream ois = new ObjectInputStream(fis);
        T object = cls.cast(ois.readObject());
        ois.close();
        fis.close();
        return object;
    }

    public void saveAnimalShelter(Shelter shelter) throws IOException {
        serializeObject(shelter);
    }

    public Shelter getSavedAnimalShelter() throws IOException, ClassNotFoundException {
        return deserializeData(Shelter.class, shelterFile.getPath());
    }
}
