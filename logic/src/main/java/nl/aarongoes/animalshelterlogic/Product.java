package nl.aarongoes.animalshelterlogic;

import nl.aarongoes.animalshelterlogic.interfaces.Sellable;

import java.io.Serializable;

public class Product implements Serializable, Sellable {
    String name;
    int price;

    public Product(String name, int price){
        this.name = name;
        this.price = price;
    }
    @Override
    public int getPrice() {
        return 0;
    }

    @Override
    public void setPrice(int price) {

    }

    public String toString() {
        return this.name + " sold for: " + this.price;
    }
}
