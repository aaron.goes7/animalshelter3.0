package nl.aarongoes.animalshelterlogic.interfaces;

//import lombok.Getter;

public interface Sellable {
    String Name = null;
    int getPrice();
    void setPrice(int price);
}
