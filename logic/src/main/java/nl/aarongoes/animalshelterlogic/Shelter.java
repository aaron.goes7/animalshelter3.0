package nl.aarongoes.animalshelterlogic;

import nl.aarongoes.animalshelterlogic.animals.Animal;
import nl.aarongoes.animalshelterlogic.animals.Gender;
import nl.aarongoes.animalshelterlogic.animals.Reservor;
import nl.aarongoes.animalshelterlogic.animals.Species;
import nl.aarongoes.animalshelterlogic.interfaces.Sellable;
import nl.aarongoes.animalshelterlogic.storage.ShelterStorage;

import java.io.Serializable;
import java.util.List;

public class Shelter implements Serializable {
    private Reservation reservation;
    private WebShop webshop;

    public Shelter(){
        reservation = new Reservation();
        webshop = new WebShop();
    }

    public List<Animal> getAnimals(){
        return reservation.getAnimals();
    }

    public List<Sellable> getItems(){
        return webshop.getItems();
    }

    public void addAnimal(Species species, String name, Gender gender, String badHabits){
        reservation.addAnimal(Factory.createAnimal(species, name, gender, badHabits));
    }

    public void sellAnimal(Animal animal){
        reservation.removeAnimal(animal);
        webshop.addAnimal(animal);
    }

    public void sellItem(Product item){
        webshop.addItem(item);
    }

    public void buyItem(Sellable item){
        webshop.removeItem(item);
    }

    public void reserveAnimal(Animal animal, String reservor){
        animal.reserve(reservor);
    }
}
