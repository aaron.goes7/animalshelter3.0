package nl.aarongoes.animalshelterlogic;

import nl.aarongoes.animalshelterlogic.animals.*;

public class Factory {
    public static Animal createAnimal(Species animalType, String name, Gender gender, String badHabits){
        switch(animalType){
            case Dog:
                return new Dog(name, gender);
            case Cat:
                return new Cat(name, gender, badHabits);
            default:
                return null;
        }
    }

    public static Species getAnimalType(Animal animal){
        if (animal instanceof Dog){
            return Species.Dog;
        }
        else if (animal instanceof Cat){
            return Species.Cat;
        }
        else{
            return null;
        }
    }
}
