package nl.aarongoes.animalshelterlogic;

import nl.aarongoes.animalshelterlogic.animals.Animal;
import nl.aarongoes.animalshelterlogic.animals.Cat;
import nl.aarongoes.animalshelterlogic.animals.Dog;
import nl.aarongoes.animalshelterlogic.animals.Gender;

import java.io.Serializable;
import java.util.*;

public class Reservation implements Serializable {
    public List<Animal> animals;

    public Reservation() {
         animals = new ArrayList<>();
    }

    public List<Animal> getAnimals(){
        return Collections.unmodifiableList(animals);
    }

    public void addAnimal(Animal animal){
        this.animals.add(animal);
    }

    public void removeAnimal(Animal animal){
        this.animals.remove(animal);
    }
}
