package nl.aarongoes.animalshelterlogic;

import nl.aarongoes.animalshelterlogic.animals.Animal;
import nl.aarongoes.animalshelterlogic.animals.Cat;
import nl.aarongoes.animalshelterlogic.animals.Dog;
import nl.aarongoes.animalshelterlogic.animals.Gender;
import nl.aarongoes.animalshelterlogic.interfaces.Sellable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class WebShop implements Serializable {
    public List<Sellable> items;

    public WebShop() {
        items = new ArrayList<>();
    }

    public List<Sellable> getItems(){
        return Collections.unmodifiableList(items);
    }

    public void addAnimal(Sellable item){
        this.items.add(item);
        item.setPrice(getPriceOfAnimal((Animal)item));
    }

    public void addItem(Sellable item){
        this.items.add(item);
    }

    public void removeItem(Sellable item){
        this.items.remove(item);
    }

    public int getPriceOfAnimal(Animal animal){
        switch(Factory.getAnimalType(animal)){
            case Dog:
                List<Dog> dogs = items
                        .stream()
                        .filter(p -> p instanceof Dog)
                        .map(p -> (Dog) p)
                        .collect(Collectors.toList());
                int dogPrice = 500 - 50 * dogs.indexOf(animal);
                if (dogPrice >= 50){
                    return dogPrice;
                }
                else{
                    return 50;
                }
            case Cat:
                var cat = (Cat)animal;
                int catPrice = 350 - 20 * cat.getBadHabits().length();
                if (catPrice >= 35){
                    return catPrice;
                }
                else{
                    return 35;
                }
            default:
                return 0;

        }
    }
}
