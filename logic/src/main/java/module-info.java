module logic {
    exports nl.aarongoes.animalshelterlogic.animals;
    exports nl.aarongoes.animalshelterlogic.interfaces;
    exports nl.aarongoes.animalshelterlogic;
    exports nl.aarongoes.animalshelterlogic.storage;
}