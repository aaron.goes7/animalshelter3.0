package nl.aarongoes.animalshelterlogic;

import nl.aarongoes.animalshelterlogic.animals.Gender;
import nl.aarongoes.animalshelterlogic.animals.Species;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ReservationTest {
    private Reservation reservation;

    @Test
    public void testNewCat(){
        this.reservation = new Reservation();
        Assertions.assertEquals(0, this.reservation.animals.size());
        this.reservation.addAnimal(Factory.createAnimal(Species.Cat,"Ms. Meow", Gender.female, "Scratches couch"));
        Assertions.assertEquals(1, this.reservation.animals.size());
    }

    @Test
    public void testNewDog()
    {
        this.reservation = new Reservation();
        Assertions.assertEquals(0, this.reservation.animals.size());
        this.reservation.addAnimal(Factory.createAnimal(Species.Dog, "Sgt. Woof", Gender.male, ""));
        Assertions.assertEquals(1, this.reservation.animals.size());
    }
}
