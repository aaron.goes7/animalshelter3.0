package nl.aarongoes.animalshelterlogic.animals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.util.Date;

public class ReservorTest {

    @Test
    public void testConstructor()
    {
        Date reservedAt = new Date();
        Reservor reservor = new Reservor("John Doe", reservedAt);
        Assertions.assertEquals("John Doe", reservor.name);
        Assertions.assertEquals(reservedAt, reservor.reservedAt);
    }
}
