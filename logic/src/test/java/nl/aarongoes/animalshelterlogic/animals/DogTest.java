package nl.aarongoes.animalshelterlogic.animals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DogTest {
    private Dog dog;

    public DogTest(){
        this.dog = new Dog("Sgt. Woof", Gender.male);
    }

    @Test
    public void testConstructor()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Assertions.assertEquals("Sgt. Woof", this.dog.name);
        Assertions.assertEquals(Gender.male, this.dog.gender);
        Assertions.assertNull(this.dog.reservedBy);
        Assertions.assertEquals(sdf.format(new Date()), sdf.format(this.dog.lastWalk));
        Assertions.assertFalse(this.dog.needsWalk());
    }

    @Test
    public void testReservation()
    {
        Assertions.assertNull(this.dog.reservedBy);
        Assertions.assertTrue(this.dog.reserve("John Doe"));
        Assertions.assertNotNull(this.dog.reservedBy);
        Assertions.assertEquals("John Doe", this.dog.reservedBy.name);
        Assertions.assertFalse(this.dog.reserve("Jane Doe"));
    }

//    @Test
//    public void testReservationTime()
//    {
//        // TODO: Implement this when interfaces have been explained.
//        // We don't yet test the reserved time because it used the current
//        // system clock to determine the result. Therefore we cannot check
//        // if the values are the same, since two consecutive calls to
//        // DateTime.Now are very likely to give different results. There is
//        // a way to account for this, but the solution will use interfaces:
//        // a concept that is not yet discussed in class. To be implemented.
//        Assertions.fail("Implement this when interfaces have been explained");
//    }
}
