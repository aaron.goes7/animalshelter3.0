package nl.aarongoes.animalshelterlogic.animals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CatTest {
    private Cat cat;

    public CatTest(){
        this.cat = new Cat("Ms. Meow", Gender.female, "Scratches couch");
    }

    @Test
    public void testConstructor()
    {
        Assertions.assertEquals("Ms. Meow", this.cat.name);
        Assertions.assertEquals(Gender.female, this.cat.gender);
        Assertions.assertNull(this.cat.reservedBy);
        Assertions.assertEquals("Scratches couch", this.cat.badHabits);
    }

    @Test
    public void testReservation()
    {
        Assertions.assertNull(this.cat.reservedBy);
        Assertions.assertTrue(this.cat.reserve("John Doe"));
        Assertions.assertNotNull(this.cat.reservedBy);
        Assertions.assertEquals("John Doe", this.cat.reservedBy.name);
        Assertions.assertFalse(this.cat.reserve("Jane Doe"));
    }

//    @Test
//    public void testReservationTime()
//    {
//        // TODO: Implement this when interfaces have been explained.
//        // We don't yet test the reserved time because it used the current
//        // system clock to determine the result. Therefore we cannot check
//        // if the values are the same, since two consecutive calls to
//        // DateTime.Now are very likely to give different results. There is
//        // a way to account for this, but the solution will use interfaces:
//        // a concept that is not yet discussed in class. To be implemented.
//        Assertions.fail("Implement this when interfaces have been explained");
//    }
}
