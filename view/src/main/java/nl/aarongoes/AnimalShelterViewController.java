package nl.aarongoes;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;

import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import nl.aarongoes.animalshelterlogic.Application;
import nl.aarongoes.animalshelterlogic.Observer;
import nl.aarongoes.animalshelterlogic.Product;
import nl.aarongoes.animalshelterlogic.Shelter;
import nl.aarongoes.animalshelterlogic.animals.Animal;
import nl.aarongoes.animalshelterlogic.animals.Gender;
import nl.aarongoes.animalshelterlogic.animals.Species;
import nl.aarongoes.animalshelterlogic.interfaces.Sellable;
import nl.aarongoes.animalshelterlogic.storage.ShelterStorage;

import java.io.IOException;
import java.util.List;

public class AnimalShelterViewController {

    private Application application;
    private ShelterStorage shelterStorage;
    public Observer observer;

    @FXML
    private ComboBox cbSpecies;
    @FXML
    private TextField tfName;
    @FXML
    private TextField tfNameReservor;
    @FXML
    private ComboBox cbGender;
    @FXML
    private TextField tfBadHabits;
    @FXML
    private ListView lvAnimals;
    @FXML
    private ListView lvItems;
    @FXML
    private TextField tfItemName;
    @FXML
    private TextField tfItemPrice;

    @FXML
    public void initialize() {
        this.application = new Application();
        this.observer = new ShelterObserver(application);
        this.cbSpecies.getItems().setAll(Species.values());
        this.cbGender.getItems().setAll(Gender.values());
    }

    @FXML
    private void sellFood() throws IOException {
        application.sellItem(new Product(tfItemName.getText(), Integer.parseInt(tfItemPrice.getText())));
    }

    @FXML
    private void updateLists(List<Animal> animals, List<Sellable> items){
        lvAnimals.setItems(FXCollections.observableArrayList(animals));
        lvItems.setItems(FXCollections.observableArrayList(items));
    }

    @FXML
    private void addAnimal(){
        application.addAnimal((Species)cbSpecies.getValue(), tfName.getText(), (Gender)cbGender.getValue(), tfBadHabits.getText());
    }

    @FXML
    private void reserveAnimal(){
        application.reserveAnimal((Animal)lvAnimals.getSelectionModel().getSelectedItem(), tfNameReservor.getText());
    }

    @FXML
    private void sellAnimal(){
        application.sellAnimal((Animal)lvAnimals.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void buyItem(){
        application.buyItem((Sellable) lvItems.getSelectionModel().getSelectedItem());
    }

    public class ShelterObserver implements Observer {
        private Application application;

        ShelterObserver(Application application) {
            this.application = application;
            application.subscribe(this);
        }

        public void update() {
            updateLists(application.getAnimals(), application.getItems());
        }
    }
}
