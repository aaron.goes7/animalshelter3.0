module nl.aarongoes {
    requires javafx.controls;
    requires javafx.fxml;
    requires logic;

    opens nl.aarongoes to javafx.fxml;
    exports nl.aarongoes;
}